from collections import OrderedDict

import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint

TAU_0 = 0
T_START = 0
T_END = 2.75
DELTA_T = 0.01
X_1_END = -33
X_2_END = 6
X_3_END = 2


def control_v(tau):
    return -tau ** 3 + 3 * tau ** 2 + 2


def x_1(tau):
    return -33 * tau ** 2


def x_2(tau):
    return 6 * tau


def x_3(tau):
    return tau ** 3 - 3 * tau ** 2 + 4


def u(tau):
    return 1 / control_v(tau) - 2


def diff_equation_tau(tau, t):
    return 1 / control_v(tau)


def plot_fun(time_plot_list, list_to_plot, y_label):
    f1 = plt.figure()
    ax1 = f1.add_subplot(111)
    ax1.plot(time_plot_list, list_to_plot, linewidth=2.5)
    ax = plt.gca()
    plt.xlabel('t', fontsize='xx-large')
    ax.xaxis.set_label_coords(1, -0.05)
    plt.ylabel(y_label, rotation=0, fontsize='xx-large')
    ax.yaxis.set_label_coords(-0.05, 1)


def find_max_err(x1, x2, x3, x1_end, x2_end, x3_end):
    return max(abs(x1_end - x1), abs(x2_end - x2), abs(x3_end - x3))


if __name__ == '__main__':
    solutions = OrderedDict()

    time_points_number = int((T_END - T_START) / DELTA_T)
    time_points = np.linspace(T_START, T_END, time_points_number)

    solutions['τ'] = odeint(diff_equation_tau, TAU_0, time_points)[:, 0]

    solutions['x₁'] = list(map(x_1, solutions['τ']))
    solutions['x₂'] = list(map(x_2, solutions['τ']))
    solutions['x₃'] = list(map(x_3, solutions['τ']))
    solutions['u'] = list(map(u, solutions['τ']))



    print(find_max_err(solutions['x₁'][-1], solutions['x₂'][-1], solutions['x₃'][-1], X_1_END, X_2_END, X_3_END))
    print(solutions['x₁'][-1], solutions['x₂'][-1], solutions['x₃'][-1], solutions['u'][-1])

    # for function_name, points in solutions.items():
    #     plot_fun(time_points, points, function_name)
    # plt.show()
