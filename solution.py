from numpy.ma import array
from scipy.optimize import fsolve
from math import e
import matplotlib.pyplot as plt

Z_11_0 = 0
Z_12_0 = 0
Z_21_0 = 0
Z_22_0 = 0
ETA_0 = 0
Z_11_END_TIME = 3
Z_12_END_TIME = 1.0 / 3
Z_21_END_TIME = 4
Z_22_END_TIME = 1.0 / 4
ETA_END_TIME = 1
DELTA = 10 ** (-4)
TAU = 10 ** (-4)


def b_1_z_11(eta):
    add = 1 / (e ** (-2) + 1) / 3
    return eta ** 2 * (9 - add) + eta ** 3 * (add - 6)


def b_2_z_21(eta):
    add = 1 / (e ** (-2) + 1) / 4
    return eta ** 2 * (12 - add) + eta ** 3 * (add - 8)


def b_1_derivative(eta):
    add = 1 / (e ** (-2) + 1) / 3
    return 2 * eta * (9 - add) + 3 * eta ** 2 * (add - 6)


def b_2_derivative(eta):
    add = 1 / (e ** (-2) + 1) / 4
    return 2 * eta * (12 - add) + 3 * eta ** 2 * (add - 8)


def q(z_12, z_22, eta):
    return e ** (-3 * z_12 - 4 * z_22) + eta ** 2


def right_diff_derivaive(z_k, z_k_next, tau):
    return (z_k_next - z_k) / tau


def system_of_equations(z, eta):
    z_12 = z[0]
    z_22 = z[1]
    f = [0, 0]
    f[0] = b_1_derivative(eta[0]) * q(z_12, z_22, eta[0]) - z_12
    f[1] = b_2_derivative(eta[0]) * q(z_12, z_22, eta[0]) - z_22
    return f


def plot_fun(time_plot_list, list_to_plot, y_label):
    f1 = plt.figure()
    ax1 = f1.add_subplot(111)
    ax1.plot(time_plot_list, list_to_plot, linewidth=2.5)
    # lines = plt.plot(time_plot_list, list_to_plot)
    ax = plt.gca()
    plt.xlabel('t')
    ax.xaxis.set_label_coords(1, -0.05)
    plt.ylabel(y_label, rotation=0)
    ax.yaxis.set_label_coords(-0.05, 1)


if __name__ == '__main__':
    eta_k = ETA_0
    eta_temp = ETA_0
    z_12_k = Z_12_0
    z_22_k = Z_22_0
    z_12_k_old = 0
    z_22_k_old = 0

    iteration = 0
    z_guess = array([0, 0])

    show_iteration = int(0.5 / TAU)
    add_point = int(0.05 / TAU)

    time_list = []
    eta_list = []
    z11_list = []
    z12_list = []
    z21_list = []
    z22_list = []
    u1_list = []
    u2_list = []

    # print('iteration', 'eta_temp')
    while 1 - eta_k > DELTA:
        iteration += 1
        eta_k = eta_temp
        eta_k_next = eta_k + TAU * q(z_12_k, z_22_k, eta_k)
        eta_temp = eta_k_next

        z = fsolve(system_of_equations, z_guess, args=[eta_k])
        [z_12_k_old, z_22_k_old] = [z_12_k, z_22_k]
        [z_12_k, z_22_k] = z
        z_guess = array(z)
        # if iteration % show_iteration == 0:
        #     print(iteration, eta_temp)
        if iteration % add_point == 0:
            z_dot_12 = right_diff_derivaive(z_12_k_old, z_12_k, TAU)
            z_dot_22 = right_diff_derivaive(z_22_k_old, z_22_k, TAU)
            u1 = (z_dot_12 + z_dot_22) / 2
            u2 = (z_dot_12 - z_dot_22) / 2

            time_list.append(iteration * TAU)
            eta_list.append(eta_k)
            z11_list.append(b_1_z_11(eta_k))
            z12_list.append(z_12_k)
            z21_list.append(b_2_z_21(eta_k))
            z22_list.append(z_22_k)
            u1_list.append(u1)
            u2_list.append(u2)

        z_guess = array(z)

    time_end = iteration * TAU

    time_list.append(time_end)
    eta_list.append(eta_k)
    z11_list.append(b_1_z_11(eta_k))
    z12_list.append(z_12_k)
    z21_list.append(b_2_z_21(eta_k))
    z22_list.append(z_22_k)

    z_dot_12 = right_diff_derivaive(z_12_k_old, z_12_k, TAU)
    z_dot_22 = right_diff_derivaive(z_22_k_old, z_22_k, TAU)
    u1 = (z_dot_12 + z_dot_22) / 2
    u2 = (z_dot_12 - z_dot_22) / 2

    u1_list.append(u1)
    u2_list.append(u2)

    print('t* =', time_end)
    print('η:', eta_k)
    print('z: ', b_1_z_11(eta_k), z_12_k, b_2_z_21(eta_k), z_22_k)

    plot_fun(time_list, eta_list, 'η')
    plot_fun(time_list, z11_list, 'z¹₁')
    plot_fun(time_list, z12_list, 'z¹₂')
    plot_fun(time_list, z21_list, 'z²₁')
    plot_fun(time_list, z22_list, 'z²₂')
    plot_fun(time_list, u1_list, 'u₁')
    plot_fun(time_list, u2_list, 'u₂')
    plt.show()




# 4.37325 - TAU = 10**(-3)/2 DELTA = 10 ** (-3)

# TAU = 10**(-4) DELTA = 10 ** (-4)
#t* = 4.374 -
# η: 0.999973246246
# z:  2.9999921391 0.333692408309 3.99999410057 0.250588084566

# 4.37405 - TAU = 10**(-4)/2 DELTA = 10 ** (-4)
# 4.3741 - TAU = 10**(-4)/2 DELTA = 10 ** (-4)/2
# 4.37415 - TAU = 10**(-4)/4 DELTA = 10 ** (-4)/4
# 4.37411 - TAU = 10**(-5) DELTA = 10 ** (-4)

#TAU = 10**(-5) DELTA = 10 ** (-4)
# t* = 4.3741900000000005
# η: 0.999993921105
# z:  2.99999821493 0.333414919476 3.999998661 0.250133648297

# 4.3742 - TAU = 10**(-5)/2 DELTA = 10 ** (-5)
# 4.374209 - TAU = 10**(-6) DELTA = 10 ** (-5)/2
