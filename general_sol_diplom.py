import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint, quad
from scipy.optimize import fsolve
from sympy import Matrix, symbols
from sympy.solvers.solveset import linsolve

TAU_0 = 0
TAU_END = 0.9384149
T_START = 0
T_END = 3
DELTA_T = 0.005

X_START = (0, 0, 4)
X_END = (-33, 6, 2)


def main():
    # z_guess = np.array([TAU_0])
    # z = fsolve(v, z_guess, args=(TAU_END,))
    # print(z)

    z_coefficients_vector = find_z_coefficients(TAU_0, TAU_END, X_START, X_END)

    time_end = quad(control_v, 0, TAU_END, args=(z_coefficients_vector,))[0]
    print('time end = ', time_end)

    time_points = get_time_set(T_START, time_end, DELTA_T)
    solutions = solve(time_points, TAU_0, z_coefficients_vector)

    print_results(solutions, z_coefficients_vector, TAU_0, TAU_END, X_END)

    for function_name, points in solutions.items():
        plot_function(time_points, points, function_name)

    plot_fun_tau(TAU_0, TAU_END, z_coefficients_vector)

    plt.show()


def count_z_from_x(x_i):
    x_1, x_2, x_3 = x_i
    z_1 = x_3 + x_1 + x_2 ** 2
    z_2 = x_1 + x_2 ** 2
    z_3 = x_2
    return z_1, z_2, z_3


def count_x_from_z(z_1, z_2, z_3):
    x_1 = z_2 - z_3 ** 2
    x_2 = z_3
    x_3 = z_1 - z_2
    return x_1, x_2, x_3


def x1(tau, a):
    z_2 = z2(tau, a)
    z_3 = z3(tau, a)
    return z_2 - z_3 ** 2


def x2(tau, a):
    return z3(tau, a)


def x3(tau, a):
    z_1 = z1(tau, a)
    z_2 = z2(tau, a)
    return z_1 - z_2


def calculate_z_coefficients(tau):
    z_1 = [tau ** 5, tau ** 4, tau ** 3, tau ** 2, tau, 1]
    z_2 = [5 * tau ** 4, 4 * tau ** 3, 3 * tau ** 2, 2 * tau, 1, 0]
    z_3 = [20 * tau ** 3, 12 * tau ** 2, 6 * tau, 2, 0, 0]
    return [z_1, z_2, z_3]


def z1(tau, a):
    return a[0] * tau ** 5 + a[1] * tau ** 4 + a[2] * tau ** 3 + a[3] * tau ** 2 + a[4] * tau + a[5]


def z2(tau, a):
    return 5 * a[0] * tau ** 4 + 4 * a[1] * tau ** 3 + 3 * a[2] * tau ** 2 + 2 * a[3] * tau + a[4]


def z3(tau, a):
    return 20 * a[0] * tau ** 3 + 12 * a[1] * tau ** 2 + 6 * a[2] * tau + 2 * a[3]


def z3_derivative(tau, a):
    return 60 * a[0] * tau ** 2 + 24 * a[1] * tau + 6 * a[2]


def find_z_coefficients(tau_0, tau_end, x_start, x_end):
    z_matrix = Matrix(calculate_z_coefficients(tau_0) + calculate_z_coefficients(tau_end))
    z_vector = Matrix(list(count_z_from_x(x_start) + count_z_from_x(x_end)))

    system = z_matrix, z_vector
    a0, a1, a2, a3, a4, a5 = symbols('a0, a1, a2, a3, a4, a5')
    a_vector = (list(linsolve(system, a0, a1, a2, a3, a4, a5))[0])
    return a_vector


def function_for_bisection(tau_end, tau_0, x_start, x_end, time_to_find):
    a_vector = find_z_coefficients(tau_0, tau_end, x_start, x_end)
    time_end = quad(control_v, 0, tau_end, args=(a_vector,))[0]
    return time_end - time_to_find


def control_v(tau, a):
    z_3_der = z3_derivative(tau, a)
    z_1 = z1(tau, a)
    z_2 = z2(tau, a)
    return z_3_der - z_1 + z_2


def v(tau, tau_end):
    t = tau_end
    return (-4 * t ** 5 + t ** 2 * (18 * t ** 2 - 72 * t + 60) +
            t * tau ** 3 * (-24 * t ** 2 + t * (-3 * t ** 2 + 12 * t - 10) + 84 * t - 60)
            - 72 * t * tau * (2 * t ** 2 - 7 * t + 5) + tau ** 5 * (-3 * t ** 2 + 9 * t - 6)
            + tau ** 4 * (15 * t ** 2 + 3 * t * (2 * t ** 2 - 7 * t + 5) - 45 * t + 30)
            + tau ** 2 * (
                t ** 2 * (9 * t ** 2 - 36 * t + 30) + 180 * t ** 2 - 540 * t + 360)) / t ** 5


def u(tau, a):
    return 1 / control_v(tau, a) - 2


def diff_equation_tau(tau, t, a):
    res = 1 / control_v(tau, a)
    return res


def diff_equation_with_counted_v(tau, t, tau_end):
    res = 1 / v(tau, tau_end)
    return res


def get_time_set(t_start, t_end, delta_t):
    time_points_number = int((t_end - t_start) / delta_t)
    return np.linspace(t_start, t_end, time_points_number)


def solve(time_points, tau_0, z_coefficients):
    tau_0 = np.array([tau_0])
    solutions = dict()
    solutions['τ'] = odeint(diff_equation_tau, tau_0, time_points, (z_coefficients,))[:, 0]
    solutions['x₁'] = [x1(tau, z_coefficients) for tau in solutions['τ']]
    solutions['x₂'] = [x2(tau, z_coefficients) for tau in solutions['τ']]
    solutions['x₃'] = [x3(tau, z_coefficients) for tau in solutions['τ']]
    solutions['u'] = [u(tau, z_coefficients) for tau in solutions['τ']]
    return solutions


def find_max_err(x_1, x_2, x_3, x_end):
    return max(abs(x_end[0] - x_1), abs(x_end[1] - x_2), abs(x_end[2] - x_3))


def print_results(solutions, a_vector, tau_0, tau_end, x_end):
    print('z end:', z1(tau_end, a_vector), z2(tau_end, a_vector), z3(tau_end, a_vector))
    print('x end:', x1(tau_end, a_vector), x2(tau_end, a_vector), x3(tau_end, a_vector))
    print('x sol:', solutions['x₁'][-1], solutions['x₂'][-1], solutions['x₃'][-1])
    print('max error = ',
          find_max_err(solutions['x₁'][-1], solutions['x₂'][-1], solutions['x₃'][-1], x_end))
    print()
    print('v(0) =', control_v(tau_0, a_vector), 'v =', control_v(tau_end, a_vector), 'u(0) = ',
          u(tau_0, a_vector),
          'u = ', u(tau_end, a_vector))
    print('для найденного τ:')
    print('v(0) = ', control_v(solutions['τ'][0], a_vector), 'v = ',
          control_v(solutions['τ'][-1], a_vector),
          'u(0) = ', solutions['u'][0], 'u = ', solutions['u'][-1], 'τ = ', solutions['τ'][-1])
    print('v в общем виде:')
    print('v(0) =', v(tau_0, tau_end), 'v =', v(tau_end, tau_end))


def plot_function(time_plot_list, list_to_plot, y_label, x_label='t'):
    f1 = plt.figure()
    ax1 = f1.add_subplot(111)
    ax1.plot(time_plot_list, list_to_plot, linewidth=2.5)
    ax = plt.gca()
    plt.xlabel(x_label, fontsize='xx-large')
    ax.xaxis.set_label_coords(1, -0.05)
    plt.ylabel(y_label, rotation=0, fontsize='xx-large')
    ax.yaxis.set_label_coords(-0.05, 1)


def plot_fun_tau(tau_0, tau_end, a_vector):
    tau_points = np.linspace(tau_0, tau_end, 100)
    t_points = np.linspace(T_START, T_END, 100)
    v_points = [v(tau, tau_end) for tau in tau_points]
    v_points2 = [control_v(tau, a_vector) for tau in tau_points]
    points1 = [z1(tau, a_vector) for tau in tau_points]
    points2 = [z2(tau, a_vector) for tau in tau_points]
    points3 = [z3(tau, a_vector) for tau in tau_points]
    plot_function(t_points, v_points2, 'v')
    plot_function(t_points, points1, 'z1')
    plot_function(t_points, points2, 'z2')
    plot_function(t_points, points3, 'z3')


if __name__ == '__main__':
    main()
